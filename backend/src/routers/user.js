const express = require('express')
const User = require('../models/User')
const auth = require('../middleware/auth')
const fs = require('fs');
const os = require('os');
const router = express.Router()

router.post('/users', async (req, res) => {
    
    // Create a new user
    try {
        const user = new User(req.body)
        await user.save()
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }
})

router.get('/readfileapi', async (req, res) => {
    try {
        fs.readFile('./src/routers/file.html', function(err, data){
            res.writeHead(200, { 'Content-Type': 'text/html'})
            res.write(data);
            res.end();
        })
        fs.writeFile('mynewfile2.txt', 'Hello content!', function (err) {
            if (err) throw err;
            console.log('Saved!');
        });
        fs.appendFile('mynewfile2.txt', ' This is my text.', function (err) {
            if (err) throw err;
            console.log('Updated!');
        });
        // Endianness
        console.log('endianness : ' + os.endianness());

        // OS type
        console.log('type : ' + os.type());

        // OS platform
        console.log('platform : ' + os.platform());

        // Total system memory
        console.log('total memory : ' + os.totalmem() + " bytes.");

        // Total free memory
        console.log('free memory : ' + os.freemem() + " bytes.");
        // Total free memory
        console.log('free memory : ' + os.cpus().toString() + " bytes.");
        
    } catch (err) {
        res.status(400).send(error)
    }
});

router.post('/users/login', async(req, res) => {
    //Login a registered user
    try {
        const { email, password } = req.body
        const user = await User.findByCredentials(email, password)
        console.log(user)
        if (!user) {
            return res.status(401).send({error: 'Login failed! Check authentication credentials'})
        }
        const token = await user.generateAuthToken()
        res.send({ user, token, loginStatus: true })
    } catch (error) {
        res.status(400).send(error)
    }

})

router.get('/users/me', auth, async(req, res) => {
    // View logged in user profile
    res.send(req.user)
})

router.post('/users/me/logout', auth, async (req, res) => {
    // Log user out of the application
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token != req.token
        })
        await req.user.save()
        res.send({alert: 'Logout Successfullly'})
    } catch (error) {
        res.status(500).send(error)
    }
})

router.post('/users/me/logoutall', auth, async(req, res) => {
    // Log user out of all devices
    try {
        req.user.tokens.splice(0, req.user.tokens.length)
        await req.user.save()
        res.send()
    } catch (error) {
        res.status(500).send(error)
    }
})

module.exports = router