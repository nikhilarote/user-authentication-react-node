const fs = require('fs');
const os = require('os');
var events = require('events');
var http = require('http');

//create a server object:
http.createServer(function (req, res) {
  res.write('Hello World!'); //write a response to the client
  res.end(); //end the response
}).listen(8080); //the server object listens on port 8080

// FS and OS modules Files system  //  

fs.readFile('./src/Modules/file.html', function(err, data){
    console.log("File Data")
})
fs.writeFile('mynewfile2.txt', 'Hello content!', function (err) {
    if (err) throw err;
    console.log('Saved!');
});
fs.appendFile('mynewfile2.txt', ' This is my text.', function (err) {
    if (err) throw err;
    console.log('Updated!');
});
// Endianness
console.log('endianness : ' + os.endianness());

// OS type
console.log('type : ' + os.type());

// OS platform
console.log('platform : ' + os.platform());

// Total system memory
console.log('total memory : ' + os.totalmem() + " bytes.");

// Total free memory
console.log('free memory : ' + os.freemem() + " bytes.");
// Total free memory
console.log('free memory : ' + os.cpus().toString() + " bytes.");



// Using Event Emitter  //

var eventEmitter = new events.EventEmitter();

var listner1 = function listner1() {
   console.log('listner1 executed.');
}

var listner2 = function listner2() {
   console.log('listner2 executed.');
}

eventEmitter.addListener('connection', listner1);

// Bind the connection event with the listner2 function
eventEmitter.on('connection', listner2);

var eventListeners = require('events').EventEmitter.listenerCount
   (eventEmitter,'connection');
console.log(eventListeners + " Listner(s) listening to connection event");

// Fire the connection event 
eventEmitter.emit('connection');

// Remove the binding of listner1 function
eventEmitter.removeListener('connection', listner1);
console.log("Listner1 will not listen now.");

// Fire the connection event 
eventEmitter.emit('connection');

eventListeners = require('events').EventEmitter.listenerCount(eventEmitter,'connection');
console.log(eventListeners + " Listner(s) listening to connection event");

console.log("Program Ended.");