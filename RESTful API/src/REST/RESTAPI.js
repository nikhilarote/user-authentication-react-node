import axios from 'axios';
let axiosConfig = {
  headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "http://localhost:9000",
      'Accept': 'application/json'
  }
}
export const AuthenticateAPI = (uname, password) => {
  var postData = {
    email: uname,
    password: password
  };
  
  return axios.post(`http://localhost:9000/users/login`, {
    email: uname,
    password: password
  }, axiosConfig);
}
export const UserDataAPI = (obj) => {
  
  return axios.post(`http://localhost:9000/users`, {
    name: obj.name,
    email: obj.email,
    password: obj.password
  }, axiosConfig);
}
