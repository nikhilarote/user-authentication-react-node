const configObj = {};

configObj.BASE_URL = "http://localhost:9000/";

configObj.headers = {
    "Accept": "application/json", 
    "Content-Type": "application/json"
};

export default configObj;
