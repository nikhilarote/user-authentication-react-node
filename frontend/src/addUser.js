import React from 'react';
import './App.css';
import { UserDataAPI } from './REST/RESTAPI';
import { useHistory, Link } from 'react-router-dom';

class AddUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
    this.submitUserData =  this.submitUserData.bind(this);
  }
  submitUserData(event) {
    event.preventDefault();
    let data = {
      name: this.state.name,
      password: this.state.password,
      email: this.state.email
    }
    UserDataAPI(data);
  }
  render() {
    return(
      <div style={{margin: '0 auto', maxWidth: 400 }}>

        <form onSubmit={this.submitUserData}>
          <h3 style={{textAlign:'center'}}>New User Form</h3>
          <div className="imgcontainer">
            <img src="img_avatar2.png" alt="Avatar" className="avatar"/>
          </div>
          <div className="container">
            <label htmlFor="name"><b>Name</b></label>
            <input type="text" placeholder="Enter Username" name="name" required onChange={(e) => {
              this.setState({name: e.target.value });
            }}/>
            <label htmlFor="email"><b>Email</b></label>
            <input type="text" placeholder="Enter Email" name="email" required onChange={(e) => {
              this.setState({email: e.target.value });
            }}/>

            <label htmlFor="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="psw" required onChange={(e) => {
              this.setState({password: e.target.value });
            }}/>
            

            <div className="container" style={{backgroundColor:"#f1f1f1"}}>
              <Link to="/"><button type="button" className="cancelbtn">Back</button></Link>
              <button type="submit" className="cancelbtn" style={{float:'right', backgroundColor:'#4CAF50'}}>Save</button>
            </div>
          </div>
        </form>
    </div>
    );
  }
}
export default AddUser;
