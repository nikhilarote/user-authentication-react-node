import React from 'react';
import './App.css';
import { AuthenticateAPI } from './REST/RESTAPI';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from "./redux/action";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      incorrect: false
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    let { uname, password } = this.state;
    let values = {
      email: uname, password: password 
    }
    this.props.triggerLoginAPI(values);
    // AuthenticateAPI(uname, password).then(response => {
    //   if(response.data.loginStatus) {
    //     this.props.history.push({
    //       pathname: '/viewUser'
    //     })
    //     document.cookie = 'token='+ response.data.token
    //   } else {
    //     this.setState({ incorrect : true })
    //   }
    // })
  }
  componentDidUpdate() {
    if(this.props.response) {
      document.cookie = "Token="+this.props.response.token;
    }
    this.props.isLoggedIn && this.props.history.push('/dashboard');
  }
  render() {
    return(
      <div style={{margin: '0 auto', maxWidth: 400 }}>
        <form>
          <h3 style={{textAlign:'center'}}>User Login</h3>
          <div className="imgcontainer">
            <img src="img_avatar2.png" alt="Avatar" className="avatar"/>
          </div>
          <div className="container">
            <label htmlFor="uname"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="uname" required onChange={(e) => {
              this.setState({uname: e.target.value });
            }}/>
            <label htmlFor="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="psw" required onChange={(e) => {
              this.setState({password: e.target.value });
            }}/>
            <br/>
            { this.state.incorrect ? <span className='error'>Incorrect credientials</span> : null } <br/>
            <button type="submit" onClick={this.handleSubmit}>Login</button>
          </div>
          <div className="container" style={{backgroundColor:"#f1f1f1"}}>
            <Link to="/"><button type="button" className="cancelbtn">Back</button></Link>
            <span className="psw">Forgot <a href="#">password?</a></span>
          </div>
        </form>
    </div>
    );
  }
}
const mapStateToProps = (state) => {
  let { isLoggedIn, response } = state.LoginReducer;
  console.log(state)
  return {
    isLoggedIn, response
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(Actions, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
