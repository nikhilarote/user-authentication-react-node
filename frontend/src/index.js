import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import Login from './LoginForm';
import Home from './Home';
import Logout from './Logout';
import AddUser from './addUser';
import ViewUser from './viewUser';
import Dashboard from './Dashboard/Dashboard';
import Profile from './Dashboard/profile';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store';
const routing = (
  <Provider store={store}>
    <App/>
    {/* <Router>
      <div>
        <Route exact path="/" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/addUser" component={AddUser}/>
          <Route path="/logout" component={Logout} />
          <Route path="/viewUser" component={ViewUser}/>
          <Route path="/Dashboard" component={Dashboard}>
          </Route>
          <Route path="/profile" component={Profile}/>
      </div>
    </Router> */}
  </Provider>
)
ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
