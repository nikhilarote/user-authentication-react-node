import config from "../../utils/config";
import {
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT
} from "./actionType";

const headers = config.headers, BASE_URL = config.BASE_URL;

async function handleAPICall(params) {
    headers.Authorization = "Bearer "+document.cookie.split("Token=")[1]
    const apiObj = {
        method: params.TYPE || "GET",
        credentials: "same-origin",
        headers
    }

    if(params.TYPE === "POST") {
        apiObj.body = JSON.stringify(params.data);
    }

    const response = await fetch(params.END_POINT, apiObj);

    if(response) {

        const resJson = await response.json();
        try {
            params.dispatch({type: params.successAction, payload: resJson, data: params.data});
        } catch (e) {
            params.dispatch({type: params.failedAction, payload: resJson});
        }
    }
}

export function triggerLoginAPI(obj) {
    return dispatch => {
        const params = {
            TYPE: 'POST',
            dispatch,
            END_POINT: BASE_URL + "users/login",
            successAction: LOGIN_SUCCESS,
            failedAction: LOGIN_FAILURE,
            data: obj
        }

        handleAPICall(params);
    }
}

export function triggerLogoutAPI(obj) {
    return dispatch => {
        const params = {
            TYPE: 'POST',
            dispatch,
            END_POINT: BASE_URL + 'users/me/logout',
            data: obj,
            successAction: LOGOUT,
            failedAction: LOGOUT,
        }
        handleAPICall(params)
    }
}