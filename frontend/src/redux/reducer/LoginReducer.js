import {
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT
} from "../action/actionType";

const initialState = {
    isLoggedIn: false,
    errorMessage: ''
};

const loginReducer = function(state = initialState, action) {
    switch(action.type) {
        case LOGIN_SUCCESS:
            let loginCreds = action.payload;
            return {
                ...state,
                response: loginCreds,
                isLoggedIn: loginCreds.loginStatus,
                message: loginCreds.loginStatus ? "" : "Invalid Credentials"
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                response: loginCreds,
                isLoggedIn: false,
                message: "Invalid Credentials"
            };
        case LOGOUT:
            return {
                ...state,
                response: null,
                isLoggedIn: false,
                message: ""
            };
        default:
            return state;

    }
};

export default loginReducer;