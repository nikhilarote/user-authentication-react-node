import React from 'react';
import Sidebar from './sidebar';
import Header from './Header';
class Dashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        this.openNav = this.openNav.bind(this);
        this.closeNav = this.closeNav.bind(this);
    }
    openNav() {
        this.setState({navWidth: '250px'});
    }
    closeNav() {
        this.setState({navWidth: '0px'});
    }
    render() {
        return (
            <div>
                <Header/>
                <Sidebar/>            
            </div>
        )
    }
}

export default Dashboard;