import React from 'react';
import { Link } from 'react-router-dom';
class Header extends React.Component {
    render() {
        return(
            <div className="header">
                <Link to="/Dashboard"><span className="logo">Logo</span></Link>
                <div className="header-right">
                    <Link to="/"><span className="active">Home</span></Link>
                    <Link to="/profile"><span>Profile</span></Link>
                    <Link to="/logout"><span>Logout</span></Link>
                </div>
            </div>
        )
    }
}

export default Header;