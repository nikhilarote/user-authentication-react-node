import React from 'react';
import { Link } from 'react-router-dom';

class Sidebar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        this.openNav = this.openNav.bind(this);
        this.closeNav = this.closeNav.bind(this);
    }
    openNav() {
        this.setState({navWidth: '250px'});
    }
    closeNav() {
        this.setState({navWidth: '0px'});
    }
    render() {
        return (
            <div>
                <div id="mySidenav" className="sidenav" style={{width: this.state.navWidth}}>
                    <a href="#" className="closebtn" onClick={this.closeNav}>&times;</a>
                </div>
                <span style={{fontSize:'30px', cursor:'pointer'}} onClick={this.openNav}>&#9776;</span>               
            </div>
        )
    }
}

export default Sidebar;