import React from 'react';
import './App.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from "./redux/action";

class Logout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentDidMount() {
    let values = {
      token: document.cookie.split("Token=")[1]
    }
    this.props.triggerLogoutAPI(values);
    this.props.history.push('/');
  }
  render() {
    return(
      <div className="Logout">Logout Successfully</div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
}
const mapDispatchToProps = (Dispatch) => {
  return bindActionCreators(Actions, Dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Logout);
