import React, { Fragment } from 'react';
import './index.css';

import Login from './LoginForm';
import Home from './Home';
import Logout from './Logout';
import AddUser from './addUser';
import ViewUser from './viewUser';
import Dashboard from './Dashboard/Dashboard';
import Profile from './Dashboard/profile';
import { connect } from 'react-redux';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import store from './redux/store';
import PropTypes from 'prop-types'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  static contextTypes = {
    router: PropTypes.object
  }
  componentDidMount() {
    // !this.props.isLoggedIn && alert('User loggedout successfully');
    //!this.props.isLoggedIn && this.props.history.push('/');
    
  }
  render() {
    return (
      <Router>
          <div>
            <Route exact path="/" component={Home} />
              <Route path="/login" component={Login} />
              <Route path="/addUser" component={AddUser}/>
              <Route path="/logout" component={Logout} />
              <Route path="/viewUser" component={ViewUser}/>
              <Route path="/Dashboard" component={Dashboard}>
              </Route>
              <Route path="/profile" component={Profile}/>
          </div>
        </Router>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.LoginReducer.isLoggedIn
  }
}
export default connect(mapStateToProps)(App);
