import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  render() {
    return(
      <div>
        <Link to="/addUser"><button type="button" className="cancelbtn">New User</button></Link>
        <span> Node Js Backend API Demo </span>
        <Link to="/login"><button type="button" className="Loginbtn">Login Page</button></Link>
      </div>
    );
  }
}
export default Home;
